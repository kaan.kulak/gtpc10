package org.mik.springhw.entity.security;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Set;

@Data
@ToString(callSuper = false)
@NoArgsConstructor
@Entity
@SuperBuilder
@Table(name = AppUser.TBL_NAME)
public class AppUser {
    public static final String TBL_NAME="users";
    public static final String FLD_USER_NAME="username";
    public static final String FLD_PASSWORD="password";
    public static final String FLD_FIRST_NAME="first_name";
    public static final String FLD_LAST_NAME="last_name";
    public static final String FLD_MAIL="mail";
    public static final String FLD_AUTHORITY="authority";
    public static final String FLD_ACTIVE="active";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = FLD_USER_NAME, nullable = false, unique = true, updatable = false)
    private String userName;

    @NotNull
    @Column(name = FLD_PASSWORD, nullable = false)
    private String password;

    @NotNull
    @Column(name = FLD_FIRST_NAME, nullable = false)
    private String firstName;

    @NotNull
    @Column(name = FLD_LAST_NAME, nullable = false)
    private String lastName;

    @NotNull
    @Column(name = FLD_MAIL, unique = true)
    private String mail;

    @Transient
    private String fullName;

    public String getFullName() {
        return firstName + ' ' + lastName;
    }

    public void setFullName(String fn) {
        if (fn==null || !fn.contains(" "))
            return;

        String parts[]=fn.split(" ");
        firstName=parts[0];
        lastName=parts[1];
    }

    @ElementCollection
    @JoinTable(
            name = "authorities",
            joinColumns = {@JoinColumn(name = "username")}
    )
    @Column(name = FLD_AUTHORITY)
    private Set<String> roles;

    @Column(name = FLD_ACTIVE)
    private Boolean active;

    public AppUser(String userName, String password, String firstName, String lastName, String mail, String fullName, Set<String> roles, Boolean active) {
        this.userName = userName;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.fullName = fullName;
        this.roles = roles;
        this.active = active;
    }
}
