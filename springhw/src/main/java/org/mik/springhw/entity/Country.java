package org.mik.springhw.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = Country.TBL_NAME)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Country {
    public static final String TBL_NAME="country";
    public static final String FLD_NAME="countryname";
    public static final String FLD_SIGN="sign";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = FLD_NAME,nullable = false)
    private String name;
    @Column(name = FLD_SIGN)
    private String sign;

    public Country(String name, String sign) {
        this.name = name;
        this.sign = sign;
    }
}
