package org.mik.springhw.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = Patient.TBL_NAME)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Patient {

    public static final String TBL_NAME="patient";
    public static final String FLD_FIRSTNAME="firstname";
    public static final String FLD_LASTNAME="lastname";
    public static final String FLD_EMAIL="email";
    public static final String FLD_DOCTOR="doctor";
    public static final String FLD_COUNTRY="country";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = FLD_FIRSTNAME,nullable = false)
    private String firstName;
    @Column(name = FLD_LASTNAME)
    private String lastName;
    @Column(name = FLD_EMAIL)
    private String email;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = FLD_DOCTOR)
    private Doctor doctor;

    @ManyToOne
    @JoinColumn(name = FLD_COUNTRY)
    private Country country;

    public Patient(String firstname, String lastName, String email,Doctor doctor,Country country) {
        this.firstName = firstname;
        this.lastName = lastName;
        this.email = email;
        this.doctor = doctor;
        this.country = country;
    }
}
