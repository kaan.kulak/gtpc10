package org.mik.springhw.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Bean;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Table(name = Doctor.TBL_NAME)
public class Doctor {
    public static final String TBL_NAME="doctor";
    public static final String FLD_FIRSTNAME="firstname";
    public static final String FLD_LASTNAME="lastname";
    public static final String FLD_PHONE="phone";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = FLD_FIRSTNAME,nullable = false)
    private String firstName;

    @Column(name = FLD_LASTNAME)
    private String lastName;

    @Column(name = FLD_PHONE)
    private String phone;

    public Doctor(String firstName, String lastName, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
    }
}
