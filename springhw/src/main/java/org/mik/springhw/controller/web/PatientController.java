package org.mik.springhw.controller.web;

import org.mik.springhw.entity.Patient;
import org.mik.springhw.service.CountryService;
import org.mik.springhw.service.DoctorService;
import org.mik.springhw.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class PatientController {
    private PatientService patientService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private CountryService countryService;

    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    // handler method to handle list students and return model and view
    @GetMapping("/patients")
    public String listPatients(Model model) {
        model.addAttribute("patients",patientService.getAllPatients());
        //return view
        return "patients";
    }

    @GetMapping("/patients/new")
    public String createPatientForm(Model model) {
        // create patient object to hold form data
        Patient patient = new Patient();
        model.addAttribute("patient",patient);
        model.addAttribute("doctors",doctorService.getAllDoctors());
        model.addAttribute("countries",countryService.getAllCountries());
        return "create_patient";
    }

    @PostMapping("/patients")
    public String savePatient(@ModelAttribute("patient") Patient patient) {
        patientService.savePatient(patient);
        return "redirect:/patients";
    }

    @GetMapping("/patients/edit/{id}")
    public String editPatientForm(@PathVariable Long id, Model model) {
        model.addAttribute("patient",patientService.getPatientById(id));
        model.addAttribute("doctors",doctorService.getAllDoctors());
        model.addAttribute("countries",countryService.getAllCountries());
        return "edit_patient";
    }

    @PostMapping("/patients/{id}")
    public String updatePatient(@PathVariable Long id,
                                @ModelAttribute("patient")Patient patient,
                                Model model) {
        // get patient from database by id
        Patient existingPatient = patientService.getPatientById(id);
        existingPatient.setId(patient.getId());
        existingPatient.setFirstName(patient.getFirstName());
        existingPatient.setLastName(patient.getLastName());
        existingPatient.setEmail(patient.getEmail());
        existingPatient.setDoctor(patient.getDoctor());

        patientService.updatePatient(existingPatient);
        return "redirect:/patients";

    }

    // delete button handler
    @GetMapping("/patients/{id}")
    public String deletePatient(@PathVariable Long id) {
        patientService.deletePatientById(id);
        return "redirect:/patients";
    }
}
