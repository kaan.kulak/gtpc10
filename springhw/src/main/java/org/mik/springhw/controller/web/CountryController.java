package org.mik.springhw.controller.web;

import org.mik.springhw.entity.Country;
import org.mik.springhw.service.CountryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CountryController {
    private CountryService countryService;

    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping("/countries")
    public String listCountries(Model model) {
        model.addAttribute("countries",countryService.getAllCountries());
        return "countries";
    }

    @GetMapping("/countries/new")
    public String createCountryForm(Model model) {
        Country country = new Country();
        model.addAttribute("country",country);
        return "create_country";
    }

    @PostMapping("/countries")
    public String saveCountry(@ModelAttribute("country") Country country) {
        countryService.saveCountry(country);
        return "redirect:/countries";
    }

    @GetMapping("/countries/edit/{id}")
    public String editCountryForm(@PathVariable Long id,Model model) {
        model.addAttribute("country",countryService.getCountryById(id));
        return "edit_country";
    }

    @PostMapping("/countries/{id}")
    public String updateCountry(@PathVariable Long id,
                                @ModelAttribute("country")Country country,
                                Model model) {
        Country existingCountry = countryService.getCountryById(id);
        existingCountry.setId(country.getId());
        existingCountry.setName(country.getName());
        existingCountry.setSign(country.getSign());

        countryService.updateCountry(existingCountry);
        return "redirect:/countries";
    }

    @GetMapping("/countries/{id}")
    public String deleteCountry(@PathVariable Long id) {
        countryService.deleteCountryById(id);
        return "redirect:/countries";
    }
}
