package org.mik.springhw.controller.web;

import org.mik.springhw.entity.Doctor;
import org.mik.springhw.service.DoctorService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class DoctorController {
    private DoctorService doctorService;

    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping("/doctors")
    public String listDoctors(Model model) {
        model.addAttribute("doctors",doctorService.getAllDoctors());
        return "doctors";
    }

    @GetMapping("/doctors/new")
    public String createDoctorForm(Model model) {
        Doctor doctor = new Doctor();
        model.addAttribute("doctor",doctor);
        return "create_doctor";
    }

    @PostMapping("/doctors")
    public String saveDoctor(@ModelAttribute Doctor doctor) {
        doctorService.saveDoctor(doctor);
        return "redirect:/doctors";
    }

    @GetMapping("/doctors/edit/{id}")
    public String editDoctorForm(@PathVariable Long id, Model model) {
        model.addAttribute("doctor",doctorService.getDoctorById(id));
        return "edit_doctor";
    }

    @PostMapping("/doctors/{id}")
    public String updateDoctor(@PathVariable Long id,
                               @ModelAttribute("doctor")Doctor doctor,
                               Model model) {
        Doctor existingDoctor = doctorService.getDoctorById(id);
        existingDoctor.setId(doctor.getId());
        existingDoctor.setFirstName(doctor.getFirstName());
        existingDoctor.setLastName(doctor.getLastName());
        existingDoctor.setPhone(doctor.getPhone());

        doctorService.updateDoctor(existingDoctor);
        return "redirect:/doctors";
    }

    @GetMapping("/doctors/{id}")
    public String deleteDoctor(@PathVariable Long id) {
        doctorService.deleteDoctorById(id);
        return "redirect:/doctors";
    }

}
