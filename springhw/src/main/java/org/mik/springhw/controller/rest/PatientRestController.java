package org.mik.springhw.controller.rest;

import org.mik.springhw.Constants;
import org.mik.springhw.entity.Patient;
import org.mik.springhw.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/patients")
public class PatientRestController {

    private PatientService patientService;

    public PatientRestController(@Autowired PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Patient> findById(Long id) {
        Patient p = patientService.getPatientById(id);
        return p==null ? ResponseEntity.notFound().build() : ResponseEntity.ok(p);
    }

    @GetMapping("/all")
    public ResponseEntity<Page<Patient>> findAll(@RequestParam(name = "page",defaultValue = "0") int page,
                                                 @RequestParam(name = "size",defaultValue = "5") int size) {
        Pageable paging = PageRequest.of(page, size);
        Page<Patient> l= patientService.getAllPatients(paging);
        return ResponseEntity.ok(l);
    }
    @PostMapping(consumes = Constants.MIME_JSON)
    public ResponseEntity<Patient> addPatient(@RequestBody Patient patient) {
        patientService.savePatient(patient);
        return ResponseEntity.ok(patient);
    }
    @PutMapping(name = "/{id}", consumes = Constants.MIME_JSON)
    public ResponseEntity<Patient> updateCountry(@RequestBody Patient patient, @PathVariable long id) {
        Patient p =patientService.getPatientById(id);
        if (p!=null) {
            p.setFirstName(patient.getFirstName());
            p.setLastName(patient.getLastName());
            p.setEmail(patient.getEmail());
            return ResponseEntity.ok(this.patientService.savePatient(p));
        }
        patient.setId(id);
        return ResponseEntity.ok(this.patientService.savePatient(patient));
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Void> deleteCountry(@PathVariable Long id) {
        Patient p=this.patientService.getPatientById(id);
        if (p==null)
            return ResponseEntity.notFound().build();
        this.patientService.deletePatientById(id);
        return ResponseEntity.ok().build();
    }
}
