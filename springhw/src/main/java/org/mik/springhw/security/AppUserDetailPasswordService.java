package org.mik.springhw.security;

import org.mik.springhw.entity.security.AppUser;
import org.mik.springhw.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsPasswordService;
import org.springframework.stereotype.Service;

@Service
public class AppUserDetailPasswordService implements UserDetailsPasswordService {
    private AppUserRepository appUserRepository;
    private AppUserDetailsMapper appUserDetailsMapper;

    public AppUserDetailPasswordService(@Autowired AppUserRepository appUserRepository,
                                        @Autowired AppUserDetailsMapper appUserDetailsMapper) {
        this.appUserRepository = appUserRepository;
        this.appUserDetailsMapper = appUserDetailsMapper;
    }

    @Override
    public UserDetails updatePassword(UserDetails user, String newPassword) {
        AppUser usr = appUserRepository.findByUserName(user.getUsername());
        usr.setPassword(newPassword);
        return appUserDetailsMapper.toUserDetails(usr);
    }
}
