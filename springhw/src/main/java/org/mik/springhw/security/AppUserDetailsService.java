package org.mik.springhw.security;

import net.bytebuddy.utility.nullability.AlwaysNull;
import org.mik.springhw.entity.security.AppUser;
import org.mik.springhw.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;

@Service
@Transactional
public class AppUserDetailsService implements UserDetailsService {
    private AppUserRepository userCredentionalRepository;
    private AppUserDetailsMapper usersDetailsMapper;

    public AppUserDetailsService(@Autowired AppUserRepository userCredentionalRepository,
                                 @Autowired AppUserDetailsMapper usersDetailsMapper) {
        this.userCredentionalRepository = userCredentionalRepository;
        this.usersDetailsMapper = usersDetailsMapper;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser usr = userCredentionalRepository.findByUserName(username);
        return usersDetailsMapper.toUserDetails(usr);
    }

}
