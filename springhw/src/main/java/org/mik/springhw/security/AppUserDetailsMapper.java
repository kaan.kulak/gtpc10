package org.mik.springhw.security;

import org.mik.springhw.entity.security.AppUser;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class AppUserDetailsMapper {
    UserDetails toUserDetails(AppUser usr) {
        return User.withUsername(usr.getUserName())
                .password(usr.getPassword())
                .roles(usr.getRoles().toArray(String[]::new))
                .build();
    }
}
