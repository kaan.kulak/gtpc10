package org.mik.springhw.repository;

import org.mik.springhw.entity.security.AppUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppUserRepository extends CrudRepository<AppUser,Long> {
    AppUser findByUserName(String username);
}
