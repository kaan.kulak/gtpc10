package org.mik.springhw.service;

import org.mik.springhw.entity.Country;

import java.util.List;

public interface CountryService {
    List<Country> getAllCountries();
    Country saveCountry(Country country);
    Country getCountryById(Long id);
    void deleteCountryById(Long id);

    Country updateCountry(Country country);
}
