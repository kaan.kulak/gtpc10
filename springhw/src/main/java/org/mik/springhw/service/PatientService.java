package org.mik.springhw.service;

import org.mik.springhw.entity.Patient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PatientService {
    List<Patient> getAllPatients();

    Page<Patient> getAllPatients(Pageable pageable);

    Patient savePatient(Patient patient);

    Patient getPatientById(Long id);
    Patient updatePatient(Patient patient);

    void deletePatientById(Long id);

}
