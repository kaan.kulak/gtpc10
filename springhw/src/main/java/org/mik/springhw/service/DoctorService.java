package org.mik.springhw.service;

import org.mik.springhw.entity.Doctor;

import java.util.List;

public interface DoctorService {
    List<Doctor> getAllDoctors();
    Doctor saveDoctor(Doctor doctor);
    Doctor getDoctorById(Long id);
    Doctor updateDoctor(Doctor doctor);
    void deleteDoctorById(Long id);
}
