package org.mik.springhw;

import org.mik.springhw.entity.Country;
import org.mik.springhw.entity.Doctor;
import org.mik.springhw.entity.Patient;
import org.mik.springhw.entity.security.AppUser;
import org.mik.springhw.repository.DoctorRepository;
import org.mik.springhw.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

@SpringBootApplication
public class SpringhwApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringhwApplication.class, args);
	}

	@Autowired
	private PatientRepository patientRepository;

	@Autowired
	private DoctorRepository doctorRepository;

	@Override
	public void run(String... args) throws Exception {


//		Doctor d1 = new Doctor("Stephen","Strange","123123123");
//		doctorRepository.save(d1);
//
//		Country c1 = new Country("Hungary","HUN");
//		Country c2 = new Country("United States","USA");
//
//		Patient p1= new Patient("Peter","Parker","spiderman@gmail.com",d1,c1);
//		patientRepository.save(p1);
//
//		Patient p2= new Patient("Wanda","Maximoff","scarlettwitch@gmail.com",d1,c2);
//		patientRepository.save(p2);


	}
}
