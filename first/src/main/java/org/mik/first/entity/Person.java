package org.mik.first.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Entity
@Table(name = Person.TBL_NAME)
@SuperBuilder
@ToString(callSuper = true)
public class Person extends Client{

    public static final String TBL_NAME = "person";
    public static final String FLD_PERSONAL_ID="personal_id";

    @NotNull
    @Size(min = 13,max = 13,message = "idNumber exactly 13 characters long.")
    @Column(name = FLD_PERSONAL_ID,unique = true,nullable = false)
    private String idNumber;


}
