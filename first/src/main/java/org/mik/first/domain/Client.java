package org.mik.first.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.export.json.JsonElement;
import org.mik.first.export.json.JsonInit;
import org.mik.first.export.json.JsonSerializable;
import org.mik.first.export.xml.XMLElement;
import org.mik.first.export.xml.XMLInıt;
import org.mik.first.export.xml.XMLSerialization;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@XMLSerialization
@JsonSerializable
public class Client {

    private static final Logger LOG= LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    @JsonElement
    @XMLElement
    private String name;

    @JsonElement
    @XMLElement
    private String address;

    @JsonElement
    @XMLElement
    private Country country;

    @XMLInıt
    public void xmlInit() {
        LOG.debug("Enter Client.xmlInit");
    }
    @JsonInit
    public void jsonInit() {LOG.debug("Enter Client.jsonInit");}
}
