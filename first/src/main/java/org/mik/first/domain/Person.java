package org.mik.first.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.mik.first.export.json.JsonElement;
import org.mik.first.export.json.JsonSerializable;
import org.mik.first.export.xml.XMLElement;
import org.mik.first.export.xml.XMLSerialization;

@Data
@ToString(callSuper = true)
@NoArgsConstructor
@SuperBuilder
@XMLSerialization
@JsonSerializable
public class Person extends Client {

    @JsonElement
    @XMLElement
    private String personalId;

}

