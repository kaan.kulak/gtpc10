package org.mik.first.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.mik.first.export.json.JsonElement;
import org.mik.first.export.xml.XMLElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Country {
    @JsonElement
    @XMLElement
    private String name;
    @JsonElement
    @XMLElement
    private String sign;
}
