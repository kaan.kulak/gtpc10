package org.mik.first.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.mik.first.export.json.JsonElement;
import org.mik.first.export.json.JsonSerializable;
import org.mik.first.export.xml.XMLElement;
import org.mik.first.export.xml.XMLSerialization;

@Data
@NoArgsConstructor
@SuperBuilder
@XMLSerialization(key = "HatedCompanies")
@JsonSerializable(key = "HatedCompanies")
public class Company extends  Client {

    @JsonElement(key = "SecretId")
    @XMLElement(key = "SecretId")
    private String taxId;

}
