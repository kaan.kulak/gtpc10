package org.mik.first.export.xml;

import com.sun.jdi.InvocationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class XMLGenerator {
    private static final Logger LOG= LogManager.getLogger();
    private static final Boolean DEBUG_TEMPORARY=false;

    private static final String LINE_SEPARATOR=System.getProperty("line.separator");


    public String convert2XML(Object object) throws XMLSerializationException  {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(LINE_SEPARATOR).append("<?xml version=\"1.0\"").append(LINE_SEPARATOR);
            process(object,sb);
            return sb.toString();
        }catch (Exception e) {
            LOG.error(String.format("XML Processing error:%s"),e);
            throw new XMLSerializationException(e.getMessage());
        }
    }

    private void process(Object object, StringBuilder sb) throws IllegalArgumentException, IllegalAccessException, InvocationException, InvocationTargetException {
        if(DEBUG_TEMPORARY)
            LOG.debug(String.format("Enter process, object:%s",object));

        checkIfSerializable(object);
        initXML(object);
        convert2String(object,sb);
    }

    private void convert2String(Object object, StringBuilder sb) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, InvocationException {
        if (object == null)
            throw new XMLSerializationException("Object is null");
        Class<?> clazz = object.getClass();
        XMLSerialization xmlSerialization = clazz.getAnnotation(XMLSerialization.class);
        String classKey="".equals(xmlSerialization.key())
            ? clazz.getSimpleName().toUpperCase()
            : xmlSerialization.key();
        sb.append('<').append(classKey).append('>').append(LINE_SEPARATOR);
        List<Field> fields = new ArrayList<>();
        Collections.addAll(fields,clazz.getDeclaredFields());
        addParentFields(clazz,fields);
        for (Field f: fields) {
            if (f.isAnnotationPresent(XMLElement.class)) {
                f.setAccessible(true);
                XMLElement element = f.getAnnotation(XMLElement.class);
                String keys = "".equals(element.key())
                    ? f.getName().toUpperCase()
                    : element.key();
                Object value = f.get(object);
                if((value != null && value.getClass().isAnnotationPresent(XMLSerialization.class))) {
                    sb.append(LINE_SEPARATOR);
                    process(value,sb);
                }
                else {
                    sb.append('<').append(keys).append('>').append(LINE_SEPARATOR);
                    sb.append(value);
                    sb.append("</").append(keys).append('>').append(LINE_SEPARATOR);
                }
            }
        }
        sb.append(LINE_SEPARATOR).append("</").append(classKey).append(">").append(LINE_SEPARATOR);
    }

    private void addParentFields(Class<?> clazz, List<Field> fields) {
        Class<?> superClass = clazz.getSuperclass();
        if(superClass.isAnnotationPresent(XMLSerialization.class)) {
            Collections.addAll(fields,superClass.getDeclaredFields());
            addParentFields(superClass,fields);
        }
    }

    private void initXML(Object object) throws IllegalAccessException, InvocationException, InvocationTargetException {
        if (object == null) {
            throw new XMLSerializationException("Object is null");
        }

        Class<?> clazz = object.getClass();
        List<Method> methods = new ArrayList<>();
            Collections.addAll(methods,clazz.getDeclaredMethods());
            for (Method m : methods) {
                if (m.isAnnotationPresent(XMLInıt.class)) {
                    m.setAccessible(true);
                    m.invoke(object);
                }
            }
    }

    private void checkIfSerializable(Object object) throws XMLSerializationException {
        if (object == null)
            throw  new XMLSerializationException("Object is null");

        Class<?> clazz = object.getClass();
        if(!clazz.isAnnotationPresent(XMLSerialization.class))
            throw new XMLSerializationException(clazz.getName() +" is not xml serializable");
    }

}
