package org.mik.first.export.xml;

import java.lang.annotation.*;

@Documented
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface XMLSerialization {
    public  String key() default "";
}
