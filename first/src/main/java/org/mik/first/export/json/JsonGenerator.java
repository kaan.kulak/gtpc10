package org.mik.first.export.json;

import com.sun.jdi.InvocationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class JsonGenerator {

    private static final Logger LOG= LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    private static final String LINE_SEPARATOR=System.getProperty("line.separator");

    public String convert2Json(Object object) throws JsonSerializationException{
        try{
            StringBuilder sb = new StringBuilder();
            sb.append(LINE_SEPARATOR).append("{").append(LINE_SEPARATOR);
            process(object,sb);
            return sb.toString();
        }catch (Exception e) {
            LOG.error(String.format("JSON Processing error:%s"),e);
            throw new JsonSerializationException();
        }
    }

    private void process(Object object,StringBuilder sb) throws IllegalArgumentException,IllegalAccessException, InvocationException, InvocationTargetException {
        if (DEBUG_TEMPORARY)
            LOG.debug(String.format("Enter process, object:%s",object));
        checkIfSerializable(object);
        initJson(object);
        convert2String(object,sb);
    }

    private void convert2String(Object object,StringBuilder sb) throws IllegalArgumentException,IllegalAccessException,InvocationTargetException,InvocationException {
        if (object == null)
            throw new JsonSerializationException("Object is null");
        Class<?> clazz = object.getClass();
        JsonSerializable jsonSerializable = clazz.getAnnotation(JsonSerializable.class);
        String classKey = "".equals(jsonSerializable.key())
                ? clazz.getSimpleName().toUpperCase()
                : jsonSerializable.key();
        sb.append('"').append(classKey).append("\":{").append(LINE_SEPARATOR);
        List<Field> fields = new ArrayList<>();
        Collections.addAll(fields,clazz.getDeclaredFields());
        addParentFields(clazz,fields);
        for (Field f: fields) {
            if (f.isAnnotationPresent(JsonElement.class)) {
                f.setAccessible(true);
                JsonElement element = f.getAnnotation(JsonElement.class);
                String keys = "".equals(element.key())
                        ? f.getName().toUpperCase()
                        : element.key();
                Object value = f.get(object);
                if ((value != null && value.getClass().isAnnotationPresent(JsonSerializable.class))) {
                    sb.append(LINE_SEPARATOR);
                    process(value,sb);
                }
                else {
                    sb.append('"').append(keys).append("\":").append(LINE_SEPARATOR);
                    sb.append(value);
                    sb.append("}").append(LINE_SEPARATOR);
                }
            }
        }
        sb.append(LINE_SEPARATOR).append("}").append(LINE_SEPARATOR);
    }

    private void addParentFields(Class<?> clazz, List<Field> fields) {
        Class<?> superClass = clazz.getSuperclass();
        if(superClass.isAnnotationPresent(JsonSerializable.class)) {
            Collections.addAll(fields,superClass.getDeclaredFields());
            addParentFields(superClass,fields);
        }
    }


    private void initJson(Object object) throws IllegalAccessException, InvocationException, InvocationTargetException {
        if (object == null) {
            throw new JsonSerializationException("Object is null");
        }

        Class<?> clazz = object.getClass();
        List<Method> methods = new ArrayList<>();
        Collections.addAll(methods,clazz.getDeclaredMethods());
        for (Method m : methods) {
            if (m.isAnnotationPresent(JsonInit.class)) {
                m.setAccessible(true);
                m.invoke(object);
            }
        }
    }

    private void checkIfSerializable(Object object) throws JsonSerializationException {
        if (object == null)
            throw  new JsonSerializationException("Object is null");

        Class<?> clazz = object.getClass();

        if(!clazz.isAnnotationPresent(JsonSerializable.class))
            throw new JsonSerializationException(clazz.getName() +" is not JSON serializable");
    }

}
