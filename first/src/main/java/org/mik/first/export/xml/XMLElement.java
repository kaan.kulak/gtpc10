package org.mik.first.export.xml;

import java.lang.annotation.*;

@Documented
@Inherited
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)

public @interface XMLElement {
    public String key() default "";
}
