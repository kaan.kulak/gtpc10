package org.mik.first.export.xml;

import lombok.AllArgsConstructor;


public class XMLSerializationException extends RuntimeException{
    public XMLSerializationException() {
    }

    public XMLSerializationException(String message) {
        super(message);
    }

    public XMLSerializationException(String message, Throwable cause) {
        super(message, cause);
    }

    public XMLSerializationException(Throwable cause) {
        super(cause);
    }

    public XMLSerializationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
