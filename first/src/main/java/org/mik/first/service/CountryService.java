package org.mik.first.service;

import org.mik.first.entity.Country;
import org.mik.first.repository.CountryRepository;

import java.util.List;

public class CountryService {

    private static CountryService instance;
    private CountryRepository repository = CountryRepository.getInstance();

    private CountryService() {

    }

    public static synchronized CountryService getInstance() {
        if (instance == null) {
            instance = new CountryService();
        }
        return instance;
    }


    public Country getBySign(String sign) throws Exception{
        return this.repository.findBySign(sign);
    }

    public long getCounts() throws Exception {
        return this.repository.getCount();
    }

    public List<Country> getAll() throws Exception {
        return this.repository.getAll();
    }
}
