package org.mik.first;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Client;

public class Main {
      private static final Logger LOG= LogManager.getLogger();
      private final static Boolean DEBUG_TEMPORARY = true;

    public static void main(String[] args) {
        LOG.trace("Enter main/TRACE");
        LOG.debug("Enter main/DEBUG");
        LOG.info("Enter main/INFO");
        LOG.warn("Enter main/WARN");
        LOG.error("Enter main/ERROR");
        LOG.fatal("Enter main/FATAL");

        new Control().start();
    }
}


