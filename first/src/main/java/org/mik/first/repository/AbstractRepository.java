package org.mik.first.repository;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.mik.first.db.HibernateUtil;
import org.mik.first.entity.AbstractEntity;
import org.mik.first.entity.Client;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity<Long>>{

    @FunctionalInterface
    interface CriteriaSettings<R,T> {
        void create(CriteriaBuilder cb, Root<T> root, CriteriaQuery<R> query);
    }

    @FunctionalInterface
    interface SqlAction<R> {
        R process(Session session, Transaction transaction);
    }

    protected abstract Class<E> getClazz();

    protected <R> R doIt(SqlAction<R> action) throws Exception {
        try(Session session = HibernateUtil.getSessionFactory().getCurrentSession()){
            Transaction tr = session.beginTransaction();
            try{
                return action.process(session,tr);
            }catch (Exception e) {
                tr.rollback();
                throw e;
            }
            finally {
                if (tr.getStatus().equals(TransactionStatus.ACTIVE))
                    tr.commit();
            }
        }
    }

    public void save(E entity) throws Exception {
        if (entity == null) {
            return;
        }
        doIt(((session, transaction) -> {
            session.save(entity);
            return entity;
        }));
    }

    public E getById(Long id) throws Exception {
        if (id == null) {
            return null;
        }
        return doIt(((session, transaction) -> session.find(getClazz(),id)));
    }

    public List<E> getByName(String name) throws Exception {
        if(name ==null|| name.isEmpty())
            return null;
        return doIt(((session, transaction) -> {
            Query<E> query = createCriteria(session,getClazz(),(cb,r,cq) -> {
                cq.select(r).where(cb.equal(r.get(Client.FLD_NAME),name));
            });
            return query.getResultList();
        }));
    }

    public List<E> getAll() throws Exception {
        return doIt(((session, transaction) -> {
            Query<E> query = createCriteria(session,getClazz(),(cb,r,cq) -> {
                cq.select(r);
            });
            return query.getResultList();
        }));
    }

    public List<E> getAll(int currentPage,int recordsPerPage) throws Exception {
        return doIt(((session, transaction) -> {
            Query<E> query = createCriteria(session,getClazz(),(cb,r,cq) -> cq.select(r));
            query.setFirstResult(currentPage*recordsPerPage);
            query.setMaxResults(recordsPerPage);
            return query.getResultList();
        }));
    }

    public void delete(Long id) throws Exception {
        if(id ==null)
            return;
        doIt(((session, transaction) -> {
            E object = session.find(getClazz(),id);
            if (object ==null) {
                return null;
            }
            session.delete(object);
            return null;
        }));
    }

    public long getCount() throws Exception {
        return doIt(((session, transaction) -> {
            Query<Long> query = createCriteria(session,Long.class,(cb,r,cq) -> {
                cq.select(cb.count(r.get(Client.FLD_ID)));
            });
            return query.getSingleResult();
        }));
    }

    public void update(E entity) throws Exception {
        if (entity == null) {
            return;
        }
        doIt(((session, transaction) -> {
            session.update(entity);
            return null;
        }));
    }

    public void saveOrUpdate(E entity) throws Exception {
        if (entity == null)
            return;

        doIt(((session, transaction) -> {
            session.saveOrUpdate(entity);
            return null;
        }));
    }

    protected <R> Query<R> createCriteria(Session session, Class<R> resultClass, CriteriaSettings<R,E> criteriaSettings) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<R> cq = cb.createQuery(resultClass);
        Root<E> root = cq.from(getClazz());
        criteriaSettings.create(cb,root,cq);
        return session.createQuery(cq);
    }
}
