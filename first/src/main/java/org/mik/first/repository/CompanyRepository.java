package org.mik.first.repository;

import org.mik.first.entity.Company;

public class CompanyRepository extends AbstractRepository<Company>{
    private static CompanyRepository instance;

    private CompanyRepository() {

    }

    public static synchronized CompanyRepository getInstance() {
        if (instance == null) {
            instance = new CompanyRepository();
        }
        return instance;
    }

    @Override
    protected Class<Company> getClazz() {
        return null;
    }
}
