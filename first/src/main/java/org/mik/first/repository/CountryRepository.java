package org.mik.first.repository;

import org.hibernate.query.Query;
import org.mik.first.entity.Client;
import org.mik.first.entity.Country;

public class CountryRepository extends AbstractRepository<Country>{


    private static CountryRepository instance;

    private CountryRepository() {

    }

    public static synchronized CountryRepository getInstance() {
        if(instance==null) {
            instance = new CountryRepository();
        }
        return instance;
    }

    public Country findBySign(String sign) throws Exception{
        if(sign ==null|| sign.isEmpty())
            return null;
        return doIt(((session, transaction) -> {
            Query<Country> query = createCriteria(session,getClazz(),(cb, r, cq) -> {
                cq.select(r).where(cb.equal(r.get(Client.FLD_NAME),sign));
            });
            return query.getSingleResult();
        }));
    }

    @Override
    protected Class<Country> getClazz() {
        return Country.class;
    }
}
