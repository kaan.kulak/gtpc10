package org.mik.first.repository;

import org.hibernate.query.Query;
import org.mik.first.entity.Client;
import org.mik.first.entity.Person;

public class PersonRepository extends AbstractRepository<Person>{
    private static PersonRepository instance;

    private PersonRepository(){

    }

    public static synchronized PersonRepository getInstance() {
        if(instance==null) {
            instance = new PersonRepository();
        }
        return instance;
    }

    @Override
    protected Class<Person> getClazz() {
        return Person.class;
    }

    public Person getByPersonalId(String pId) throws Exception {
        if(pId ==null|| pId.isEmpty())
            return null;
        return doIt(((session, transaction) -> {
            Query<Person> query = createCriteria(session,getClazz(),(cb,r,cq) -> {
                cq.select(r).where(cb.equal(r.get(Client.FLD_NAME),pId));
            });
            return query.getSingleResult();
        }));
    }
}
