package org.mik.first.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import org.mik.first.entity.Client;
import org.mik.first.entity.Company;
import org.mik.first.entity.Country;
import org.mik.first.entity.Person;
import org.mik.first.repository.CompanyRepository;


import org.mik.first.repository.CountryRepository;
import org.mik.first.repository.PersonRepository;

import java.util.Properties;

public class HibernateUtil {

    private static final Logger LOG= LogManager.getLogger();
    private final static Boolean DEBUG_TEMPORARY = true;

    private static SessionFactory sessionFactory;

    public synchronized static SessionFactory getSessionFactory(){
        if(sessionFactory == null) {
            Configuration cfg = new Configuration();
            Properties props = new Properties();
            props.put(Environment.DRIVER,"org.hsqldb.jdbc.JDBCDriver");
            props.put(Environment.URL,"jdbc:hsqldb:mem:db/mp;hsqldb.log_data=false");
            props.put(Environment.USER,"sa");
            props.put(Environment.PASS,"");
            props.put(Environment.DIALECT,"");
            props.put(Environment.SHOW_SQL,"true");
            props.put(Environment.CURRENT_SESSION_CONTEXT_CLASS,"thread");
            props.put(Environment.HBM2DDL_AUTO,"create-drop");
            props.put(Environment.ENABLE_LAZY_LOAD_NO_TRANS,"true");
            cfg.setProperties(props);


            cfg.addAnnotatedClass(Client.class);
            cfg.addAnnotatedClass(Country.class);
            cfg.addAnnotatedClass(Person.class);
            cfg.addAnnotatedClass(Company.class);

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(cfg.getProperties()).build();
            sessionFactory=cfg.buildSessionFactory(serviceRegistry);
            initDb();
        }
        return sessionFactory;
    }

    private static void initDb() {
        try {
            initCountries();
            initPersons();
            initCompanies();
        }catch (Exception e) {
            LOG.error(e);
        }

    }

    private static void initCountries() throws Exception {
        createCountry("Hungary","H");
        createCountry("USA","US");
        createCountry("Ukraine","U");
        createCountry("Earth","ETH");
        createCountry("United Kingdom","UK");
        createCountry("Betelgeuse","BET");
    }

    private static Country createCountry(String name, String sign) throws Exception {
        Country c = new Country(name,sign);
        CountryRepository r = CountryRepository.getInstance();
        r.save(c);
        return c;

    }


    private static void initPersons() throws Exception{
        createPerson("Zaphod Beeblebrox","Betelgeuse","42424242");
        createPerson("Ford Prefect","UK","4343434343");
        createPerson("Tricia McMilan","UK","4444444");
    }

    private static Person createPerson(String name, String country, String personalId)
    throws Exception{
        Country c = CountryRepository.getInstance().findBySign(country);
        Person p = Person.builder()
                .name(name)
                .country(c)
                .idNumber(personalId)
                .build();
        PersonRepository r = PersonRepository.getInstance();
        r.save(p);
        return p;
    }

    private static Company createCompany(String name, String country, String taxNumber)
    throws Exception{
        Country cy = CountryRepository.getInstance().findBySign(country);
        Company c = Company
                .builder()
                .name(name)
                .country(cy)
                .taxNumber(taxNumber)
                .build();
        CompanyRepository r = CompanyRepository.getInstance();
        r.save(c);
        return c;

    }

    private static void initCompanies() throws Exception{
        createCompany("Google","US","123456789");
    }
}
