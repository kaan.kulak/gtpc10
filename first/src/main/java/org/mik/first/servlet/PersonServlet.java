package org.mik.first.servlet;

import org.hibernate.SessionFactory;
import org.mik.first.db.HibernateUtil;
import org.mik.first.entity.Country;
import org.mik.first.entity.Person;
import org.mik.first.service.CountryService;
import org.mik.first.service.PersonService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.Optional;
import java.util.stream.Stream;

public class PersonServlet extends HttpServlet {

    private CountryService countryService;
    private PersonService personService;

    public PersonServlet() {
        countryService = CountryService.getInstance();
        personService = PersonService.getInstance();
        SessionFactory sf = HibernateUtil.getSessionFactory();
    }

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
            throws ServletException, IOException {
        String name = Optional.ofNullable(httpServletRequest.getCookies())
                .map(Stream::of)
                .orElseGet(Stream::empty)
                .filter(c->"name".equals(c.getName()))
                .findFirst()
                .map(Cookie::getValue)
                .orElse(null);

        this.handle(httpServletResponse,name);
    }

    @Override
    protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
            throws ServletException, IOException {
        String name = httpServletRequest.getParameter("name");
        String personalId = httpServletRequest.getParameter("pId");
        String countryId = httpServletRequest.getParameter("cId");
        try {
            Country c = countryService.getBySign(countryId);
            if (c == null) {
                httpServletResponse.sendRedirect("error");
                return;
            }
            Person p = this.personService.getByPersonalId(personalId);
            if (p == null) {
                httpServletResponse.sendRedirect("error");
                return;
            }
            Cookie cookie = new Cookie("name", URLEncoder.encode(p.getName(),"UTF-8"));
            httpServletResponse.sendRedirect("personList");
        }catch (Exception e) {
            httpServletResponse.sendRedirect("error");
        }
    }

    private void handle(HttpServletResponse response, String name) throws IOException{
        PrintWriter wr = response.getWriter();
        wr.println("<html><head></head><body>");
        if (name == null)
            wr.println("<h1>Hello"+name+"</h1>");
        wr.println("<form method=\"POST\">");
        wr.println("Name: <input type=\"text\" name=\"name\"/>");
        wr.println("PersonalId: <input type=\"text\" name=\"pId\"/>");
        wr.println("Country: <input type=\"text\" name=\"cId\"/>");
        wr.println("<input type=\"submit\"/>");
        wr.println("</form></body></html>");
    }
}
