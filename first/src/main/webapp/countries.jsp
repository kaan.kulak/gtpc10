<%--
  Created by IntelliJ IDEA.
  User: kaan
  Date: 4/5/22
  Time: 10:45 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="org.mik.first.service.CountryService" %>
<%@page import="java.util.List" %>
<%@page import="org.mik.first.entity.Country" %>
<%
    CountryService cs = CountryService.getInstance();
    Long nocs = cs.getCounts();
    List<Country> countries = cs.getAll();
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h1>List of countries.</h1>
    <table>
        <tr>
            <th>No.</th>
            <th>Id</th>
            <th>Name</th>
            <th>Sign</th>
        </tr>
        <%
            for (Country c: countries) {
        %>
        <tr>
            <td><%=countries.indexOf(c)%></td>
            <td><%=c.getId()%></td>
            <td><%=c.getName()%></td>
            <td><%=c.getSign()%></td>
        </tr>
        <%
            }
        %>
        <tr>
            <td colspan="5"><% out.print("Number of countries: "+nocs);%></td>
        </tr>
    </table>
    <a href="index.jsp">Main Menu</a>
</body>
</html>
